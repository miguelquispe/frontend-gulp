/*
* Dependencias
*/
var gulp = require('gulp'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    connect = require('gulp-connect'),
    autoprefixer = require('gulp-autoprefixer');
/*
* Paths
*/

var path = {
  jade: ['src/jade/**/*.jade', '!src/jade/layout.jade'],
  sass: ['src/sass/**/*.scss'],
  html: 'public/',
  assets: 'public/assets/'
};


/*
* Tasks
*/
gulp.task('html', function () {
  return gulp.src(path.jade)
    .pipe(jade({ pretty:true }))
    .pipe(gulp.dest(path.html))
    .pipe(connect.reload());
});

gulp.task('styles', function () {
  return gulp.src('src/sass/style.scss')
  .pipe(sass({outputStyle: 'expanded'}))
  .pipe(autoprefixer({ browsers: ['last 3 version'], cascade: false }))
  .pipe(gulp.dest(path.assets + 'css/'))
  .pipe(connect.reload());
});


/*
* Default
*/
gulp.task('default', ['html']);


/*
* Server
*/
gulp.task('connect', function(){

  connect.server({
      root: 'public',
      port: '9000',
      livereload: true
  });

});

/*
* Watch
*/
gulp.task('watch', function () {
  gulp.watch(path.jade, ['html']);
  gulp.watch(path.sass, ['styles']);
});
